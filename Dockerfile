FROM python
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install --upgrade pip && pip install -r requirements.txt
COPY main.py main.py
COPY model.pickle model.pickle
EXPOSE 7501
CMD ["python3", "main.py"]
from flask import Flask, request, jsonify
import pickle
import numpy as np

app = Flask(__name__)
model = pickle.load(open("model.pickle", "rb"))

@app.route("/get_score", methods=['POST'])
def get_score():
    data = request.get_json(force=True)
    x = np.array([data["features"]])
    scores = model.predict_proba(x)
    res = {"scores": list(scores[0])}
    return jsonify(res)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=7501)
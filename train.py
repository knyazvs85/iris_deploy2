from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score
import numpy as np
import pickle

dataset = datasets.load_iris()

data = dataset.data
target = dataset.target

X_train, X_test, y_train, y_test = train_test_split(data, target, test_size=0.1, random_state=42, shuffle=True)
print(data)

model = RandomForestClassifier(n_estimators=5, max_depth=5, random_state=42)

model.fit(X_train, y_train)
pred_train = model.predict(X_train)
pred_test = model.predict(X_test)

print("before_dump_pred", model.predict_proba(np.array([[7.2, 3.,  5.8, 1.6]])))

print("train_f1", f1_score(y_train, pred_train, average="macro"), "test_f1", f1_score(y_test, pred_test, average="macro"))

pickle.dump(model, open('model.pickle', "wb"))

model = pickle.load(open("model.pickle", "rb"))
print("loaded model score", f1_score(y_test, model.predict(X_test), average="macro"))

print("after_dump_pred", model.predict_proba(np.array([[5, 5,  5, 1]])))